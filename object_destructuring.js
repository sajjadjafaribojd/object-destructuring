/* jshint esversion:6 */

var a,b,rest;

[a,b]=[10,12];

console.log(a); // 10
console.log(b); // 12

[a,b,...rest]=[10,20,30,40,50,60,70]; // just 3 .(dot) befor rest(...rest)

console.log(rest); // [30,40,50,60,70]

var user={name:'JabJ',age:29};
var {name}=user;
var {age}=user;

console.log(name); // jabj
console.log(age);